---
title: "Set Up K8s Cluster"
description: ""
lead: ""
date: 2023-06-19T22:42:21+08:00
lastmod: 2023-06-19T22:42:21+08:00
draft: false
images: []
menu:
  docs:
    parent: ""
weight: 999
toc: true
---

本次实践是基于：[Kubernetes 入门实战课（17 更真实的云原生：实际搭建多节点的Kubernetes集群）](https://time.geekbang.org/column/intro/100114501)


在windows11系统，通过virtualbox 创建master worker 虚拟机
创建虚拟机，参考指南：{{< ref "create-ubuntu-server.md" >}} 

master配置：
1 memory: 4G
2 CPU: 8核
3 虚拟硬盘：60G
4 network: 桥接网卡

worker配置：
1 memory: 2G
2 CPU: 4核
3 虚拟硬盘：40G
4 network: 桥接网卡


# master节点

1 安装docker

```
sudo apt install -y docker.io #安装Docker Engine
sudo systemctl start docker         #启动docker服务

# docker ps 提示权限不足，将用户加入到docker group
sudo usermod -aG docker ${USER}   #当前用户加入docker组
newgrp docker # override the current primary group
```


2 prepare
```
# set hostname to master/worker
vim /etc/hostname
# download k8s_study
git clone https://github.com/chronolaw/k8s_study.git
cd k8s_study/admin
sh prepare.sh
```

3 install kubeadm
`sh admin.sh`

4 下载 Kubernetes 组件镜像
`sh images.sh`

5 安装master节点
修改master.sh， 增加 --apiserver-advertise-address=192.168.0.117（master桥接网卡ip地址， ip addr查看）

![](images/install-master.png)
修改完之后，执行脚本
`sh master.sh`
记录输出信息中kubeadm join部分。 worker节点需要运行这个命令
```shell
Then you can join any number of worker nodes by running the following on each as root:
kubeadm join 192.168.0.117:6443 --token tv9mkx.tw7it9vphe158e74 \
  --discovery-token-ca-cert-hash sha256:e8721b8630d5b562e23c010c70559a6d3084f629abad6a2920e87855f8fb96f3
```

6 安装 Flannel 网络插件
`kubectl apply -f flannel.yml`

# worker 节点

1 安装docker

2 prepare

3 install kubeadm

4 下载 Kubernetes 组件镜像
按照上面master节点的4个步骤，worker节点进行安装

5 安装完成之后，运行上面kubeadm join命令
```
kubeadm join 192.168.0.117:6443 --token tv9mkx.tw7it9vphe158e74 \
  --discovery-token-ca-cert-hash sha256:e8721b8630d5b562e23c010c70559a6d3084f629abad6a2920e87855f8fb96f3
```

---
master,worker节点安装完之后，在master节点运行
`kubectl get nodes`

![](images/get-nodes.png)

大功告成！！！

# Q&A
1 当桥接网卡设置固定IP时，出现flannel无法连接worker节点的问题，导致创建pod失败


