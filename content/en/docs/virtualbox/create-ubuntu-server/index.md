---
title: "Create the ubuntu Server22.04 VM"
description: ""
lead: ""
date: 2023-06-19T12:36:35+08:00
lastmod: 2023-06-19T12:36:35+08:00
draft: false
images: []
weight: 999
menu:
  docs:
    parent: ""
toc: true
---

在window11环境下, 使用virutalbox创建虚拟机

# prerequisite
- download [virtualbox7.0.8](https://www.virtualbox.org/wiki/Downloads "virtualbox7.0.8") and install
- download [ubuntu server 22.04](https://ubuntu.com/download/server "ubuntu server 22.04")

# virtual 新建虚拟机

1 新建虚拟电脑
需要配置的内容
- 名称
- 文件夹
- 虚拟光盘
![新建虚拟电脑](images/new-virtual.png)

2 自动安装
可以修改用户名和密码
![自动安装](images/auto-install.png)
3 硬件配置
内存设置为4G CPU设置为8核
![硬件配置](images/hardware-config.png)
4 虚拟硬盘
设置为40G
![虚拟硬盘](images/virtual-harddisk.png)
5 点击完成
![](images/abstract.png)

# Ubuntu安装过程
Ubuntu详细安装过程可以参考：https://hibbard.eu/install-ubuntu-virtual-box/
我这边在安装过程，基本上选择的都是提供的默认值

以下是个人配置部分：
1 点击Continue
![](images/storage-configuration.png)
2 配置Profile
![](images/profile-setup.png)
3 SSH Setup部分，选择Install OpenSSH Server。因为需要宿主机上通过ssh连接虚拟机
![](images/abstract.png) 


# Ubntun配置
1 网路配置： 选择桥接网卡
注意刷新mac地址，避免重复
![](images/ubuntu-network-config.png)
2 启动虚拟机：
```
sudo apt update
sudo apt upgrade
```
如果出现了 Daemons using outdated libaries 对话框:
用Tab按键选择OK
![](images/profile-setup.png)
3 查看虚拟机IP地址
`ip addr`
![](images/ip-addr.png)

4 wsl通过ssh连接虚拟机
window11系统安装了wsl, 通过wsl ssh连接虚拟机
![](images/ssh-connect.png)

为什么使用Terminal终端连接虚拟机，而不是直接在虚拟机上进行命令行操作？
1 虚拟机tty 支持的功能受限，不能鼠标复制，鼠标移动在主机和虚拟机之间，需要热键切换等等。
使用起来效率太低了， 而Terminal满足我所有的要求
Terminal下载地址： https://learn.microsoft.com/en-us/windows/terminal/install


---
# 常见问题：
1 使用的桥接网络，怎么设置固定IP呢？
桥接网络默认使用DHCP分配IP地址，如果要修改固定IP，按照下面流程
1 check  DHCP已经分配了一个IP地址
![](images/static-ip-addr.png)

2 vim /etc/netplan/00-installer-config.yaml
![](images/netplan-network-config-before.png)
change to 

![](images/netplan-network-config-after.png)

3 update
```
sudo netplan generate
sudo netplan apply 
```
