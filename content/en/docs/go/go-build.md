---
title: "Geek: Go Build"
description: "Tony Bai Go语言第一课"
date: 2023-06-19T16:36:16+08:00
lastmod: 2023-06-19T16:36:16+08:00
draft: false
images: []
---

```zsh
-> go version
go version go1.20.1 linux/amd64
```

使用Go Module构建模式

1 create go.mod file: `go mod init <module_path>`

 exmaple：`go mod init github.com/bigwhite/module-mode`

2 `go mod tidy`: 扫描Go源码，自动找出项目依赖的外部Go Module以及版本，下载依赖并更新本地go.mod文件

3 `go build`: 执行新module的构建

## 深入Go Module构建模式
### 1 语义导入版本：Semantic Import Versioning

v1.2.3: 1 主版本major， 2 次版本minor， 3补丁版本patch

语义版本规范： 
- 主版本不同不兼容
- 相同主版本，次版本向后兼容
- 补丁版本不影响兼容

同时依赖一个包的不兼容版本：
```
import (
	"github.com/sirupsen/logrus"
	logv2 "github.com/sirupsen/logrus/v2"
)
```
v0, v1 采用不带主版本的包导入路径： `import "github.com/sirupsen/logrus"`

### 2 Go Module 最小版本选择原则
选择符合项目整体要求的"最小版本"