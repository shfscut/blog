---
title: "Geek: {{ replace .Name "-" " " | title }}"
description: "Tony Bai Go语言第一课"
lead: ""
date: {{ .Date }}
lastmod: {{ .Date }}
draft: false
images: []
menu:
  docs:
    parent: ""
    identifier: "{{ .Name }}-{{ delimit (shuffle (split (md5 .Name) "" )) "" }}"
weight: 999
toc: true
---
